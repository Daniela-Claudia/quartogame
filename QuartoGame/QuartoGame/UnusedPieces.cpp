#include"UnusedPieces.h"

void UnusedPieces::Initialize()
{
	this->Emplace(Piece(Piece::Color::Black, Piece::Shape::Round, Piece::Height::Short, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Color::Black, Piece::Shape::Round, Piece::Height::Short, Piece::BodyStyle::Hollow));
	this->Emplace(Piece(Piece::Color::Black, Piece::Shape::Round, Piece::Height::Tall, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Color::Black, Piece::Shape::Round, Piece::Height::Tall, Piece::BodyStyle::Hollow));

	this->Emplace(Piece(Piece::Color::Black, Piece::Shape::Square, Piece::Height::Short, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Color::Black, Piece::Shape::Square, Piece::Height::Short, Piece::BodyStyle::Hollow));
	this->Emplace(Piece(Piece::Color::Black, Piece::Shape::Square, Piece::Height::Tall, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Color::Black, Piece::Shape::Square, Piece::Height::Tall, Piece::BodyStyle::Hollow));

	this->Emplace(Piece(Piece::Color::White, Piece::Shape::Round, Piece::Height::Short, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Color::White, Piece::Shape::Round, Piece::Height::Short, Piece::BodyStyle::Hollow));
	this->Emplace(Piece(Piece::Color::White, Piece::Shape::Round, Piece::Height::Tall, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Color::White, Piece::Shape::Round, Piece::Height::Tall, Piece::BodyStyle::Hollow));

	this->Emplace(Piece(Piece::Color::White, Piece::Shape::Square, Piece::Height::Short, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Color::White, Piece::Shape::Square, Piece::Height::Short, Piece::BodyStyle::Hollow));
	this->Emplace(Piece(Piece::Color::White, Piece::Shape::Square, Piece::Height::Tall, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Color::White, Piece::Shape::Square, Piece::Height::Tall, Piece::BodyStyle::Hollow));

}

void UnusedPieces::Emplace(const Piece& piece)
{
	std::stringstream ss;
	ss << piece;
	m_unusedPieces.insert(std::make_pair(ss.str(), piece));
}

Piece UnusedPieces::choosePiece(const std::string& key)
{
	auto extracted = m_unusedPieces.extract(key);
	if (extracted )
		return std::move(extracted.mapped());
	else
		throw"Piece not found!";
}

UnusedPieces::UnusedPieces()
{
	Initialize();
}

std::ostream& operator<<(std::ostream& output, const UnusedPieces& up)
{
	for (auto unusedPiece : up.m_unusedPieces)
		output << unusedPiece.first << " ";
	output << "\n";
	return output;
}
