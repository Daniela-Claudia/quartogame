#include"Piece.h"
#include"Board.h"
#include"UnusedPieces.h"
int main()
{
	UnusedPieces unusedPiece;
	Board board;
	std::cout << "Empty board:\n";
	std::cout << board;
	std::cout << std::endl;

	std::cout << "Initialize pieces:\n";
	std::cout << unusedPiece;
	std::cout << std::endl;

	board[{0, 0}] = unusedPiece.choosePiece("1101");

	std::cout << "Not empty board:\n";
	std::cout << board;
	std::cout << std::endl;

	std::cout << "Current pieces:\n";
	std::cout << unusedPiece;
	std::cout << std::endl;
	
	return 0;
}