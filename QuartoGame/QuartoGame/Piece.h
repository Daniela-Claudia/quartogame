#pragma once
#include<iostream>
#include<cstdint>
class Piece
{
public:
	enum class Color : uint8_t
	{
		Black,
		White
	};

	enum class Shape : uint8_t
	{
		Round,
		Square
	};

	enum class Height : uint8_t
	{
		Short,
		Tall
	};

	enum class BodyStyle : uint8_t
	{
		Full,
		Hollow
	};

	Piece(Color color, Shape shape, Height height, BodyStyle bodyStyle);
	Piece() = default;

	Color getColor() const;
	Shape getShape() const;
	Height getHeight() const;
	BodyStyle getBodyStyle() const;
	
	friend std::ostream& operator<<(std::ostream& Print, const Piece& piece);

private:
	Color m_Color : 1;
	Shape m_Shape : 1;
	Height m_Height : 1;
	BodyStyle m_BodyStyle : 1;
};

