#include "Board.h"
#include<tuple>

std::optional<Piece>& Board::operator[](const Position& positionReceived)
{
	uint8_t line;
	uint8_t column;
	std::tie(line, column)=positionReceived;
	//const auto&[line, column] = positionReceived;
	return m_board[line * 4 + column];
}

const std::optional<Piece>& Board::operator[](const Position& positionReceived) const
{
	uint8_t line ;
	uint8_t column ;
	std::tie(line, column) = positionReceived;
	//const auto&[line, column] = positionReceived;
	return m_board[line * 4 + column];
}

std::ostream& operator<<(std::ostream& Print, const Board& m_board)
{
	Board::Position pos;
	auto& [line, column] = pos;
	for (line = 0; line < 4; line++)
	{
		for (column = 0; column < 4; column++)
		{
			if (m_board[pos])
				Print << *m_board[pos] << " ";
			else
				Print << " ____ ";
		}
		Print << std::endl;
	}
	return Print;
}
