#pragma once
#include<array>
#include<optional>
#include"Piece.h"

class Board
{
private:
	std::array<std::optional<Piece>, 16>m_board;

public:
	using Position = std::pair<uint8_t, uint8_t>;
	std::optional<Piece>& operator[](const Position& );
	const std::optional<Piece>& operator[](const Position& ) const;
	friend std::ostream& operator<<(std::ostream& Print, const Board&);
};