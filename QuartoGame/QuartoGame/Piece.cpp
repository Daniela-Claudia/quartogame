#include "Piece.h"

Piece::Piece(Color color, Shape shape, Height height, BodyStyle bodyStyle)
	: m_Color(color), m_Shape(shape), m_Height(height), m_BodyStyle(bodyStyle)
{

}

Piece::Color Piece::getColor() const
{
	return this->m_Color;
}

Piece::Shape Piece::getShape() const
{
	return this->m_Shape;
}

Piece::Height Piece::getHeight() const
{
	return this->m_Height;
}

Piece::BodyStyle Piece::getBodyStyle() const
{
	return this->m_BodyStyle;
}

std::ostream& operator<<(std::ostream& Print, const Piece& piece)
{
	return Print << static_cast<uint16_t>(piece.m_Color) << static_cast<uint16_t>(piece.m_Shape)
		<< static_cast<uint16_t>(piece.m_Height) << static_cast<uint16_t>(piece.m_BodyStyle);
}
